"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import quantity from './forms/quantity.js' // input number
import Spoilers from "./components/spoilers.js";
import Sorter from "./components/sorter.js";

import  { hide, show, toggle } from 'slidetoggle';
import SimpleBar from 'simplebar'; // Кастомный скролл
import Choices from 'choices.js'; // Select plugin
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import HystModal from 'hystmodal';
import AirDatepicker from 'air-datepicker';
import StickySidebar from 'sticky-sidebar'

const body = document.querySelector('body')

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// input Number
quantity()

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Sorter
Sorter();

// Custom Select
document.querySelectorAll('.pretty-select').forEach(el => {
    const prettySelect = new Choices(el,{
        allowHTML: true,
        searchEnabled: false
    });
});

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
	document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
		new SimpleBar(scrollBlock, {
			autoHide: false
		});
	});
}

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Modal HystModal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
    beforeOpen: function(modal){
    },
});

/* Модуль работы с ползунком */
/*
Документация плагина: https://refreshless.com/nouislider/ */
import "./forms/range.js";

// Sliders
import "./components/sliders.js";

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-add-favorite]')) {
        event.target.closest('[data-add-favorite]').classList.toggle('added')
    }
})

// Form field
function inputsField(selector = '[data-field]') {
    const inputsFieldArray = document.querySelectorAll(selector)

    if (inputsFieldArray.length > 0) {
        inputsFieldArray.forEach(elem => {
            let inputField = elem.querySelector('input, textarea')
            let inputFieldPlaceholder = '';
            let inputFieldPlaceholderValue =  inputField.getAttribute('placeholder')

            if(inputField.hasAttribute('required')){
                inputFieldPlaceholder = '<span class="field__placeholder field__placeholder--required">' + inputFieldPlaceholderValue + '</span>'
            }
            else {
                inputFieldPlaceholder = '<span class="field__placeholder">' + inputFieldPlaceholderValue + '</span>'
            }

            elem.insertAdjacentHTML('beforeEnd', inputFieldPlaceholder)

            inputField.onfocus = function () {
                elem.classList.add('field--focus')
            }
            inputField.onblur  = function () {
                if(!inputField.value) {
                    elem.classList.remove('field--focus')
                }
            }
        })
    }
}

// file form
function fileField() {
    const fileInputs = document.querySelectorAll('[data-file]')
    if (fileInputs.length > 0) {
        fileInputs.forEach(el => {
            let fileInput = el.querySelector('[data-file-input]')
            let filePlaceholder = el.querySelector('[data-file-placeholder]')
            let fileName = ''

            fileInput.addEventListener('change', (event) => {
                fileName = fileInput.value.replace(/^.*[\\\/]/, '')
                if (fileName) {
                    filePlaceholder.innerHTML = fileName
                    el.classList.add('file-field--uploaded')
                }
                else {
                    filePlaceholder.innerHTML = filePlaceholder.dataset.filePlaceholder
                    el.classList.remove('file-field--uploaded')
                }
            })
        })
    }
}


// Collection
document.addEventListener('click', (event) => {
    if (event.target.closest('[data-collection-switcher]')) {
        const collection = event.target.closest('[data-collection]')
        const collectionTarget = event.target.closest('[data-collection-switcher]').dataset.collectionSwitcher;

        collection.querySelectorAll('[data-collection-switcher]').forEach(elem => {
            elem.classList.remove('active');
        });
        event.target.closest('[data-collection-switcher]').classList.add('active')

        collection.querySelectorAll('[data-collection-target]').forEach(elem => {
            elem.classList.remove('active');
        });
        collection.querySelectorAll(`[data-collection-target="${collectionTarget}"]`).forEach(elem => {
            elem.classList.add('active');
        });
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-text-toggle]'))  {
        const textContent = event.target.closest('[data-text]').querySelector('[data-text-content]')

        event.target.closest('[data-text-toggle]').classList.toggle('open')
        textContent.classList.toggle('open')

        if (textContent.classList.contains('open')) {
            textContent.style.maxHeight = textContent.scrollHeight + 'px';
        }
        else {
            textContent.style.maxHeight = '588px';
        }
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-cart-toggle]')) {
        const cartCollapse = event.target.closest('[data-cart]').querySelector('[data-cart-collapse]')
        toggle(
            cartCollapse,
            {
                miliseconds: 200,
            }
        )
        event.target.closest('[data-cart-toggle]').classList.toggle('close')
    }
})

// View switcher
document.addEventListener('click', (event) => {
    if (event.target.closest('[data-view-button]')) {
        let isTile = event.target.closest('[data-view-button]').dataset.viewButton
        event.target.closest('[data-view-switcher]').querySelectorAll('[data-view-button]').forEach(elem => {
            elem.classList.remove('active');
        });
        event.target.closest('[data-view-button]').classList.add('active')
        if (isTile === '1') {
            document.querySelector('[data-catalog]').classList.add('tiles-grid')
        }
        else {
            document.querySelector('[data-catalog]').classList.remove('tiles-grid')
        }
    }
})

// filter

function filter() {

    document.addEventListener('click', (event) => {

        // mobileFilter
        if (document.querySelector('[data-filter-mobile]')) {
            const mobileFilter = document.querySelector('[data-filter-mobile]');

            function mobileFilterClose() {
                body.classList.remove('scroll-disable')
                mobileFilter.classList.remove('open')
            }

            if (event.target.closest('[data-filter-toggle]')) {
                body.classList.toggle('scroll-disable')
                mobileFilter.classList.toggle('open')
            }

            if (event.target.closest('[data-filter-mobile-parent]')) {
                const mobileChildItem = event.target.closest('[data-filter-mobile-parent]').dataset.filterMobileParent
                mobileFilter.querySelector('[data-filter-mobile-primary]').classList.add('hide')
                mobileFilter.querySelector(`[data-filter-mobile-child="${mobileChildItem}"]`).classList.add('active');

            }

            if (event.target.closest('[data-filter-mobile-back]')) {
                mobileFilter.querySelectorAll('[data-filter-mobile-child]').forEach(elem => {
                    elem.classList.remove('active');
                });
                mobileFilter.querySelector('[data-filter-mobile-primary]').classList.remove('hide')
            }

            if (event.target.closest('[data-filter-submit]')) {
                mobileFilterClose()
            }

            if (event.target.closest('[data-filter-reset]')) {
                mobileFilterClose()
            }
        }

        // filter
        if (event.target.closest('[data-filter-header]')) {
            let filterGroup = event.target.closest('[data-filter-group]')
            let filterContent    = filterGroup.querySelector('[data-filter-content]');

            toggle(
                filterContent,
                {
                    miliseconds: 200,
                }
            )
            filterGroup.querySelector('[data-filter-header]').classList.toggle('open')
        }
    })
}


function orderOptions() {
    const orderOptionsArray = document.querySelectorAll('[data-order-option]')
    if (orderOptionsArray.length > 0) {

        orderOptionsArray.forEach(elem => {
            elem.addEventListener('change', (event) => {
                const orderDataContent = event.target.closest('[data-order]').querySelectorAll('[data-order-content]')

                orderDataContent.forEach(el => {
                    el.classList.remove('active')
                })
                event.target.closest('[data-order]').querySelector(`[data-order-content="${event.target.dataset.orderOption}"]`).classList.add('active');
            })
        });
    }
}

// Menu

// Items

if(document.querySelectorAll('[data-item-media]').length > 0) {

    document.querySelectorAll('[data-item-layout]').forEach(elem => {
        elem.onmouseenter = () => {
            const itemMedia = elem.closest('[data-item-media]')
            const itemMediaBullets = itemMedia.querySelectorAll('[data-item-bullet]')
            let itemMediaNum = elem.dataset.itemLayout
            itemMediaBullets.forEach(el => {
                el.classList.remove('active');
            });
            itemMedia.querySelector(`[data-item-bullet="${itemMediaNum}"]`).classList.add('active');
        }
    });

    document.querySelectorAll('[data-item-media]').forEach(elem => {
        elem.onmouseleave = () => {
            const itemMediaBullets = elem.querySelectorAll('[data-item-bullet]')
            itemMediaBullets.forEach(el => {
                el.classList.remove('active');
            });
            elem.querySelector('[data-item-bullet="1"]').classList.add('active');
        }
    });
}

function mainMenu() {
    if(document.querySelector('[data-menu]')) {
        const menu          = document.querySelector('[data-menu]')
        const menuLayout    = document.querySelector('.menu-layout')
        const menuToggle    = document.querySelector('[data-menu-toggle]')
        const menuParents   = menu.querySelectorAll('[data-menu-primary]')
        const menuChild     = menu.querySelectorAll('[data-menu-secondary]')

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-menu-toggle]')) {
                let windowInnerWidth = document.documentElement.clientWidth

                if(windowInnerWidth < 1300) {
                    document.querySelector('[data-mobile-nav]').classList.add('mobile-nav--open')
                    document.querySelector('body').classList.add('scroll-disable')
                }
                else {
                    menu.classList.toggle('menu--open')
                    menuToggle.classList.toggle('open')
                    menuLayout.classList.toggle('open')
                }
            }
        })

        menuParents.forEach(elem => {
            elem.addEventListener('mouseenter', (event) => {

                menuParents.forEach(el => {
                    el.classList.remove('active');
                });
                elem.classList.add('active')
                let menuItem = event.target.dataset.menuPrimary


                menuChild.forEach(el => {
                    el.classList.remove('active');
                });
                menu.querySelector(`[data-menu-secondary="${menuItem}"]`).classList.add('active');
            })
        });


        document.addEventListener('click', (event) => {

            if (event.target.closest('[data-mobile-nav-parent]')) {
                const navChildItem = event.target.closest('[data-mobile-nav-parent]').dataset.mobileNavParent
                document.querySelector('[data-mobile-nav-primary]').classList.add('hide')
                document.querySelector(`[data-mobile-nav-child="${navChildItem}"]`).classList.add('active');
            }

            if (event.target.closest('[data-mobile-nav-back]')) {
                document.querySelectorAll('[data-mobile-nav-child]').forEach(elem => {
                    elem.classList.remove('active');
                });
                document.querySelector('[data-mobile-nav-primary]').classList.remove('hide')
            }

            if (event.target.closest('[data-mobile-nav-close]')) {
                document.querySelectorAll('[data-mobile-nav-child]').forEach(elem => {
                    elem.classList.remove('active');
                });
                document.querySelector('[data-mobile-nav-primary]').classList.remove('hide')
                document.querySelector('[data-mobile-nav]').classList.remove('mobile-nav--open')
                document.querySelector('body').classList.remove('scroll-disable')
            }
        })
    }
}

// additional filter

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-additional-toggle]')) {
        event.target.closest('[data-additional-toggle]').classList.toggle('open')
        const additionalContent = event.target.closest('[data-modal-filter]').querySelector('[data-additional-filter]')

        toggle(
            additionalContent,
            {
                miliseconds: 200,
            }
        )
    }
})

window.addEventListener("load", function (e) {

    inputsField()
    fileField()
    orderOptions()
    filter()
    mainMenu()

    document.querySelectorAll('[data-date]').forEach(el => {
        new AirDatepicker(el,{
            autoClose: true,
            inline: false,
            timepicker: true,
            onSelect(event) {
                document.querySelector('[data-date]').closest('[data-field]').classList.add('field--focus')
            }
        });
    });

    if (document.querySelector('[data-sticky-block]')) {

        let sidebar = new StickySidebar('[data-sticky-block]', {
            topSpacing: 20,
            bottomSpacing: 20,
            minWidth: 1300,
            containerSelector: '[data-sticky]',
            innerWrapperSelector: '.main-sidebar__inner'
        });
    }
});


