export default () => {

    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-mobile-goggle]')) {
            document.querySelector('body').classList.toggle('top-bar--open')
            return false
        }
    })
};

