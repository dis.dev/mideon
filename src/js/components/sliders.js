/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, EffectFade, Navigation, Pagination, Thumbs} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-primary]')) {
        const primarySlider =  new Swiper('[data-primary]', {
            modules: [Navigation, Autoplay],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 800,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: '[data-primary-next]',
                prevEl: '[data-primary-prev]',
            },
            on: {
                slideChange: function () {
                    thumbsSlider.slideTo(this.realIndex, 800)
                }
            }
        })

        const thumbsSlider =  new Swiper('[data-primary-thumbs]', {
            modules: [Navigation, Autoplay],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 800,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            on: {
                slideChange: function () {
                    primarySlider.slideTo(this.realIndex, 800)
                }
            }
        })
    }

    if (document.querySelector('[data-hits]')) {
        new Swiper('[data-hits]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 20,
            speed: 800,
            navigation: {
                nextEl: '[data-hits-next]',
                prevEl: '[data-hits-prev]',
            },
            breakpoints: {
                768: {
                    spaceBetween: 28,
                },
                1024: {

                },
            },
        })
    }

    if (document.querySelector('[data-item-media-ddd]')) {
        new Swiper('[data-item-media-dddd]', {
            modules: [Pagination, EffectFade],
            effect: 'fade',
            observer: true,
            loop: false,
            nested: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 800,
            pagination: {
                el: "[data-item-media-pagination]",
                clickable: true,
            },
        })
    }

    if (document.querySelector('[data-collection]')) {
        const collectionArray = document.querySelectorAll('[data-collection-slider]')
        if (collectionArray.length > 0) {

            collectionArray.forEach(elem => {

                const collectionQuerySize  = 1300;
                let collectionSlider = null;

                function collectionSliderInit() {
                    if(!collectionSlider) {
                        collectionSlider =  new Swiper(elem, {
                            observer: true,
                            loop: false,
                            nested: true,
                            observeParents: true,
                            slidesPerView: 'auto',
                            spaceBetween: 20,
                            speed: 800,
                            breakpoints: {
                                768: {
                                    spaceBetween: 16,
                                    slidesPerView: 3,
                                },
                                1024: {
                                    spaceBetween: 12,
                                    slidesPerView: 4,
                                },
                                1300: {
                                    spaceBetween: 20,
                                    slidesPerView: 4,
                                },
                                1580: {
                                    spaceBetween: 30,
                                    slidesPerView: 4,
                                },
                            },
                        })
                    }
                }

                function collectionSliderDestroy() {
                    if(collectionSlider) {
                        collectionSlider.destroy();
                        collectionSlider = null;
                    }
                }

                if (document.documentElement.clientWidth < collectionQuerySize) {
                    collectionSliderInit()
                }

                window.addEventListener('resize', function (){

                    if (document.documentElement.clientWidth < collectionQuerySize) {
                        collectionSliderInit()
                    }
                    else {
                        collectionSliderDestroy()
                    }
                });
            });
        }
    }

    if (document.querySelector('[data-gallery]')) {

        const productThumbs = new Swiper('[data-thumbs]', {
            modules: [Thumbs, Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            watchSlidesProgress: true,
            slidesPerView: 3,
            spaceBetween: 11,
            speed: 600,
            navigation: {
                nextEl: '[data-thumbs-next]',
                prevEl: '[data-thumbs-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 24,
                    direction: "vertical",
                },
                1580: {
                    slidesPerView: 3,
                    spaceBetween: 25,
                    direction: "vertical",
                }
            },
        })

        const productGallery = new Swiper('[data-gallery]', {
            modules: [Thumbs, Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 600,
            thumbs: {
                swiper: productThumbs,
            },
        })
    }

    if (document.querySelector('[data-media]')) {

        const productMediaThumbs = new Swiper('[data-media-thumbs]', {
            modules: [Thumbs],
            observer: true,
            loop: false,
            observeParents: true,
            watchSlidesProgress: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            speed: 600,
            breakpoints: {
                768: {
                    slidesPerView: 4,
                    spaceBetween: 25,
                    direction: "vertical",
                },
                1300: {
                    slidesPerView: 4,
                    spaceBetween: 16,
                    direction: "horizontal",
                },
                1870: {
                    slidesPerView: 5,
                    spaceBetween: 24,
                    direction: "vertical",
                }
            },
        })

        const productMediaGallery = new Swiper('[data-media-gallery]', {
            modules: [Thumbs, Navigation, Pagination],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 600,
            pagination: {
                el: "[data-media-pagination]",
                clickable: true,
            },
            thumbs: {
                swiper: productMediaThumbs,
            }
        })
    }

    // Product reviews
    if (document.querySelector('[data-reviews]')) {
        new Swiper('[data-reviews]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 800,
            navigation: {
                nextEl: '[data-reviews-next]',
                prevEl: '[data-reviews-prev]',
            },
            breakpoints: {
                576: {
                    slidesPerView: 'auto',
                },
                1300: {
                    slidesPerView: 3,
                },
            },
        })
    }

    if (document.querySelector('[data-product-reviews]')) {
        new Swiper('[data-product-reviews]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 800,
            navigation: {
                nextEl: '[data-product-reviews-next]',
                prevEl: '[data-product-reviews-prev]',
            },
            breakpoints: {
                576: {
                    slidesPerView: 'auto',
                },
                1300: {
                    slidesPerView: 3,
                },
            },
        })
    }

    if (document.querySelector('[data-comments]')) {
        new Swiper('[data-comments]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 20,
            speed: 800,
            navigation: {
                nextEl: '[data-comments-next]',
                prevEl: '[data-comments-prev]',
            },
            breakpoints: {
                768: {
                    spaceBetween: 30,
                },
                1300: {
                    slidesPerView: 5,
                    spaceBetween: 62,
                },
                1580: {
                    slidesPerView: 6,
                    spaceBetween: 66,
                },
            },
        })
    }

    if (document.querySelector('[data-room]')) {
        new Swiper('[data-room]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 2,
            spaceBetween: 20,
            speed: 800,
            navigation: {
                nextEl: '[data-room-next]',
                prevEl: '[data-room-prev]',
            },
            breakpoints: {
                768: {
                    spaceBetween: 30,
                },
                1024: {
                    slidesPerView: 3,
                },
                1300: {
                    slidesPerView: 4,
                }
            },
        })
    }

    if (document.querySelector('[data-awards]')) {
        new Swiper('[data-awards]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 20,
            speed: 800,
            navigation: {
                nextEl: '[data-awards-next]',
                prevEl: '[data-awards-prev]',
            },
            breakpoints: {
                768: {
                    spaceBetween: 30,
                },
                1300: {
                    spaceBetween: 52,
                },
                1580: {
                    spaceBetween: 60,
                }
            }
        })
    }

    if (document.querySelector('[data-tv]')) {
        new Swiper('[data-tv]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 30,
            speed: 800,
            navigation: {
                nextEl: '[data-tv-next]',
                prevEl: '[data-tv-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                },
                1024: {
                    slidesPerView: 3,
                }
            }
        })
    }


    if (document.querySelector('[data-perfection]')) {
        const perfection = new Swiper('[data-perfection]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 800,
            navigation: {
                nextEl: '[data-perfection-next]',
                prevEl: '[data-perfection-prev]',
            },
            on: {
                init: function () {
                    this.el.querySelector('[data-perfection-total]').innerHTML = this.slides.length
                },
                slideChange: function () {
                    this.el.querySelector('[data-perfection-active]').innerHTML = '0' + (this.realIndex + 1)
                }
            }
        })
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
