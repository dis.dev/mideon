export default () => {

    if (document.querySelectorAll('[data-sorter]').length > 0) {
        const sorterArray = document.querySelectorAll('[data-sorter]');

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-sorter-button]')) {
                let sorter = event.target.closest('[data-sorter]');
                if (sorter.classList.contains('open')) {
                    sorter.classList.remove('open');
                }
                else {
                    sorterClose()
                    sorter.classList.add('open');
                }
            }
            else if (event.target.closest('[data-sorter-item]')) {
                const sorterContainer = event.target.closest('[data-sorter]')
                sorterContainer.querySelector('[data-sorter-active]').innerHTML = event.target.closest('[data-sorter-item]').dataset.sorterItem
                sorterContainer.querySelectorAll('[data-sorter-item]').forEach(el => {
                    el.classList.remove('active');
                })
                event.target.closest('[data-sorter-item]').classList.add('active')
                sorterClose();
            }

            else {
                if (!event.target.closest('[data-sorter-content]')) {
                    sorterClose();
                }
            }

            function sorterClose() {
                sorterArray.forEach(el => {
                    el.classList.remove('open');
                });
            }
        })
    }
};
